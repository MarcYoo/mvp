package com.mvp;

import com.mvp.model.ItemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yoomanyeon
 */

public class Data {

    public static ArrayList<ItemModel> getData() {
        ArrayList<ItemModel> itemModels = new ArrayList<ItemModel>();
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/03/22/1521696691372_thumbnail.jpg", "Since March 1998.", "Cookpad is the best place "));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/18/1518957234263_thumbnail.jpg", "While focusing primarily on recipe sharing and searching, our services also include meal planning and cooking videos to add to your cooking enjoyment.", "ABOUT COOKPAD LTD\n" +
                "Cookpad is the largest recipe sharing community in the world and we believe we can build a better world through encouraging even more people to cook. Every day home cooking has a profound impact on ourselves and the world around us: it makes us healthier, connects us with our friends and family, and makes our environment more sustainable. Through solving the problems related to everyday cooking, we help people live happier and healthier lives. That’s how we want to change the world. https://info.cookpad.com/en/careers"));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/18/1518957138549_thumbnail.jpg", "Open access to search, browse, and save recipes from a selection of over 20,000 publications (as of December 2016) featuring popular magazines, books, and creations of culinary researchers.", "DESCRIPTION\n" +
                "Cookpad is the world’s largest recipe sharing platform where over 100 million global users come to enjoy cooking each month in 18+ languages.\n" +
                "\n" +
                "We believe we can build a better world through encouraging even more people to cook. Everyday, home cooking has a profound impact on ourselves and the world around us: it makes us healthier and connects us with our friends and family. Through solving the problems related to everyday cooking, we help people live happier and healthier lives in an environment that is more sustainable. That’s how we want to change the world.\n" +
                "\n" +
                "People join us because they share our vision to improve people’s lives. We hire smart people who thrive in small, highly collaborative, and energised teams, and who look at what we do and want to be part of it. Uniquely, we have a mature product in Japan but, in the global market-space, we are a start-up team with a new product vision so there is freedom to be innovative and put forward brave ideas.\n" +
                "\n" +
                "Today we are helping millions of people a month and as far as we’re concerned, we’ve barely got started. If you want to work on a product making a global impact then we want to hear from you.\n" +
                "\n" +
                "As a senior Android Engineer you will:\n" +
                "\n" +
                "Develop Cookpad’s global application in collaboration with the international team\n" +
                "Participate in all phases of development, from design to implementation, testing, and release\n" +
                "Design and implement new user-facing features\n" +
                "Investigate and resolve performance issues, bottlenecks, and inefficiencies\n" +
                "Write clean, maintainable code while rapidly iterating and shipping\n" +
                "Contribute to the open-source community"));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/15/1518700543455_thumbnail.jpg", "Application to document and share bento lunch box photos with recipe", "We believe we can build a better world through encouraging more people to cook.\n" +
                "\n" +
                "Everyday, home cooking has a profound impact on ourselves and the world around us: it makes us healthier, connects us with our friends and family, and makes our environment more sustainable.\n" +
                "\n" +
                "Through solving the problems related to everyday cooking, we help people live happier and healthier lives.\n" +
                "\n" +
                "We believe that creating and empowering a community of cooks who learn and inspire each other is the way we will make a difference. Cookpad is already the largest recipe sharing community in the world with over 1 million recipes created by 100 million monthly users active in 60 countries.\n" +
                "\n" +
                "But our work has only just started and we have huge ambitions. Wehaverecentlymovedour international headquarters to Bristol, England and are looking for passionate, smart and innovative product managers, designers and engineers who want to create something extraordinary."));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/12/1518429494549_thumbnail.jpg", "For questions regarding investing in Cookpad, please use this form here.", "About Bristol\n" +
                "\n" +
                "Renowned as a colourful cultural melting pot, Bristol is one of the UK’s most exciting and vibrant cities.\n" +
                "\n" +
                "In 2017 it was awarded the ‘best place to live in Britain’ by the Sunday Times\n" +
                "\n" +
                "And is firmly established as one of the UK’s most important technology destinations for top talent."));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/12/1518429383488_thumbnail.jpg", "If you’re interested in Cookpad’s advertising options, please use this form here.", "Since washoku’s registration as a UNESCO intangible cultural heritage in December 2013, global interest in Japan’s culinary culture has been steadily increasing. In recent years, ramen restaurants and Japanese curry shops have begun expanding overseas, further indicating growing familiarity with Japanese food around the world. Visitors to Washoku.Guide will be able to explore a collection of 20,000 recipes comprised of content translated from Cookpad as well as recipes exclusive to the site. The vast selection spans from centuries-old dishes to new ideas including the currently popular onigirazu (an onigiri-sandwich hybrid) and pull-apart buns. Washoku.Guide also plans on expanding its selection of recipes that have been specially adapted for tools and ingredients available in countries outside of Japan."));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/12/1518429339499_thumbnail.jpg", "Yebisu Garden Place Tower 12th Floor\n" +
                "4-20-3 Ebisu, Shibuya-ku, Tokyo, 150-6012 Japan", "While examining traditional components of Japanese food such as dashi and miso, articles on Washoku.Guide will also explore fresh approaches to washoku such as those exemplified by the ramen burger and tofu cream. Furthermore, in addition to featuring posts written by overseas contributors, Washoku.Guide plans to share restaurant reviews and notifications of events, providing a diverse array of content that will help washoku fans all over the world cook, eat, and learn about Japanese food."));
        itemModels.add(new ItemModel( "https://dibiup-static-product.s3.ap-northeast-2.amazonaws.com/users/uploaded/feed_img_thumbnail/2018/02/12/1518410910332_thumbnail.jpg", "Cookpad is less than 10 minutes’ walk from Bristol Temple Meads Station with a frequent local train service and intercity links to London, Exeter and the South West and Glasgow and the North via Manchester, Birmingham and Leeds.", "Washoku.Guide is a portal site for washoku-related information. While sharing a collection of over 20,000 recipes, it will feature articles on the newest food trends and provide access to updated resources on Japanese food around the world.\n" +
                "\n" +
                "＜Selection of content＞\n" +
                "・Recipes:Through keyword searches and category browsing, visitors to the site can explore a rich collection of 20,000 recipes comprised of Washoku.Guide-exclusive content as well as translated recipes from Cookpad.\n" +
                "*Additions to this collection of recipes are planned in the future.\n" +
                "\n" +
                "・Articles:Articles on Washoku.Guide will range in topic from the culinary traditions of Japan to new developments in the world of washoku and include posts written by overseas contributors. Reviews of restaurants and coverage of washoku-related events in major cities around the globe are also forthcoming."));

        return itemModels;
    }


}
