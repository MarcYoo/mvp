package com.mvp;

/**
 * Created by yoomanyeon
 */

public class KeyConstants {
    public static final String URL_KEY = "url_key";
    public static final String TEXT_KEY = "text_key";
    public static final String STATE_KEY = "state_key";
}
