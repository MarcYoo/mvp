package com.mvp;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.mvp.injection.component.ApplicationComponent;
import com.mvp.injection.component.DaggerApplicationComponent;
import com.mvp.injection.module.ApplicationModule;
import com.mvp.injection.module.NetworkModule;
import com.mvp.network.NetworkConstants;
import com.mvp.setting.SettingManager;

/**
 * Created by manyeon
 */

public class MVP extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
        Fresco.initialize(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static MVP get(Context context) {
        return (MVP)context.getApplicationContext();
    }

    private void initApplicationComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(NetworkConstants.SERVER_URL, new SettingManager(this)))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

}
