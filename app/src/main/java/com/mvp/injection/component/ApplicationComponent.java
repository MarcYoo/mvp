package com.mvp.injection.component;

import com.mvp.injection.module.ApplicationModule;
import com.mvp.injection.module.ItemActivityModule;
import com.mvp.injection.module.LoginActivityModule;
import com.mvp.injection.module.NetworkModule;
import com.mvp.injection.module.ProjectsActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by yoomanyeon
 */

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                NetworkModule.class
        }
)

public interface ApplicationComponent {
    LoginActivityComponent plus(LoginActivityModule module);
    ProjectsActivityComponent plus(ProjectsActivityModule module);
    ItemActivityComponent plus(ItemActivityModule module);
}
