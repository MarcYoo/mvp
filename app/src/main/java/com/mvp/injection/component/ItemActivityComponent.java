package com.mvp.injection.component;

import com.mvp.injection.module.ItemActivityModule;
import com.mvp.injection.scope.ActivityScope;
import com.mvp.view.activities.ItemActivity;

import dagger.Subcomponent;

/**
 * Created by yoomanyeon
 */

@ActivityScope
@Subcomponent(
        modules = ItemActivityModule.class
)
public interface ItemActivityComponent {
    ItemActivity inject(ItemActivity itemActivity);
}
