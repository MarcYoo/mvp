package com.mvp.injection.component;

import com.mvp.injection.module.LoginActivityModule;
import com.mvp.injection.scope.ActivityScope;
import com.mvp.view.activities.LoginActivity;

import dagger.Subcomponent;

/**
 * Created by yoomanyeon
 */

@ActivityScope
@Subcomponent(
        modules = LoginActivityModule.class
)
public interface LoginActivityComponent {
    LoginActivity inject(LoginActivity loginActivity);
}
