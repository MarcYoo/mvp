package com.mvp.injection.component;

import com.mvp.injection.module.ProjectsActivityModule;
import com.mvp.injection.scope.ActivityScope;
import com.mvp.view.activities.ProjectsActivity;

import dagger.Subcomponent;

/**
 * Created by yoomanyeon
 */

@ActivityScope
@Subcomponent(
        modules = ProjectsActivityModule.class
)
public interface ProjectsActivityComponent {
    ProjectsActivity inject(ProjectsActivity projectsActivity);
}
