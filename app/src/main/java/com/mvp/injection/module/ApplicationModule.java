package com.mvp.injection.module;

import android.app.Application;

import com.mvp.setting.SettingManager;
import com.mvp.util.UtilityManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by yoomanyeon
 */

@Module
public class ApplicationModule {
    Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    public SettingManager provideSettingManager() {
        return new SettingManager(mApplication.getApplicationContext());
    }

    @Provides
    @Singleton
    public UtilityManager provideUtilityManager() {
        return new UtilityManager(mApplication.getApplicationContext());
    }
}
