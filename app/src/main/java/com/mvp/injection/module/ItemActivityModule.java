package com.mvp.injection.module;

import com.mvp.injection.scope.ActivityScope;
import com.mvp.injection.scope.ItemRetrofit;
import com.mvp.presenter.activities.project.ItemPresenter;
import com.mvp.setting.SettingManager;
import com.mvp.util.UtilityManager;
import com.mvp.view.activities.ItemActivity;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by yoomanyeon
 */

@Module
public class ItemActivityModule {
    private ItemActivity mActivity;

    public ItemActivityModule(ItemActivity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityScope
    public ItemActivity provideItemActivity() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    public ItemPresenter provideItemPresenter(UtilityManager utilityManager) {
        return new ItemPresenter(utilityManager);
    }

}
