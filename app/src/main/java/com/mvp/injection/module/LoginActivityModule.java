package com.mvp.injection.module;

import com.mvp.injection.scope.ActivityScope;
import com.mvp.injection.scope.AuthRetrofit;
import com.mvp.presenter.activities.login.LoginPresenter;
import com.mvp.setting.SettingManager;
import com.mvp.view.activities.LoginActivity;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by yoomanyeon
 */

@Module
public class LoginActivityModule {
    private LoginActivity mActivity;

    public LoginActivityModule(LoginActivity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityScope
    public LoginActivity provideLoginActivity() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    public LoginPresenter provideLoginPresenter(@AuthRetrofit Retrofit retrofit, SettingManager settingManager) {
        return new LoginPresenter(retrofit, settingManager);
    }
}
