package com.mvp.injection.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mvp.injection.scope.AuthRetrofit;
import com.mvp.injection.scope.ProjectsRetrofit;
import com.mvp.model.factory.ProjectsTypeAdapterFactory;
import com.mvp.model.factory.TokenGsonTypeAdapterFactory;
import com.mvp.network.Auth;
import com.mvp.network.NetworkConstants;
import com.mvp.setting.SettingManager;
import com.mvp.util.DevLog;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yoomanyeon
 */

@Module
public class NetworkModule {
    String mServerUrl;
    SettingManager mSettingManager;

    public NetworkModule(String serverUrl, SettingManager settingManager) {
        mServerUrl = serverUrl;
        mSettingManager = settingManager;
    }

    @Provides
    public Auth provideAuth() {
        return new Auth(mSettingManager);
    }

    @Provides
    @Singleton
    public Interceptor provideInterceptor(Auth auth) {
        return chain -> {
            final Request.Builder builder = chain.request().newBuilder()
                    .header("Content-Type", NetworkConstants.CONTENT_TYPE)
                    .addHeader("API_Version", NetworkConstants.API_VERSION)
                    .addHeader("Authorization", auth.toString());
            return chain.proceed(builder.build());
        };
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Interceptor interceptor) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(DevLog.BODY ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.connectTimeout(NetworkConstants.CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(NetworkConstants.WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(NetworkConstants.READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(interceptor);
        return builder.build();
    }

    @Provides
    @Singleton
    @AuthRetrofit
    public Retrofit provideAuthRetrofit(OkHttpClient okHttpClient) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(TokenGsonTypeAdapterFactory.create())
                .create();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mServerUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @ProjectsRetrofit
    public Retrofit provideProjectsRetrofit(OkHttpClient okHttpClient) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(ProjectsTypeAdapterFactory.create())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(mServerUrl)
                .client(okHttpClient)
                .build();
    }
}
