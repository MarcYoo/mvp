package com.mvp.injection.module;

import com.mvp.injection.scope.ActivityScope;
import com.mvp.injection.scope.ProjectsRetrofit;
import com.mvp.presenter.activities.projects.ProjectsPresenter;
import com.mvp.setting.SettingManager;
import com.mvp.view.activities.ProjectsActivity;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by yoomanyeon
 */

@Module
public class ProjectsActivityModule {
    private ProjectsActivity mActivity;

    public ProjectsActivityModule(ProjectsActivity activity) {
        mActivity = activity;
    }

    @Provides
    @ActivityScope
    public ProjectsActivity provideProjectsActivity() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    public ProjectsPresenter provideProjectsPresenter(@ProjectsRetrofit Retrofit retrofit, SettingManager settingManager) {
        return new ProjectsPresenter(retrofit, settingManager);
    }
}
