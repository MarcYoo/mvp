package com.mvp.injection.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by yoomanyeon
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface ProjectsRetrofit {
}
