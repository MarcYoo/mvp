package com.mvp.model;

import java.io.Serializable;

/**
 * Created by yoomanyeon
 */

public class ItemModel implements Serializable {
    private String thumbnailUrl;
    private String comment;
    private String description;

    public ItemModel(String pThumbnailUrl, String pComment, String pDescription) {
        thumbnailUrl = pThumbnailUrl;
        comment = pComment;
        description = pDescription;
    }

    public void setTumbnailUrl(String pThumbnailUrl) {
        thumbnailUrl = pThumbnailUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setComment(String pComment) {
        comment = pComment;
    }

    public String getComment() {
        return comment;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public String getDescription() {
        return description;
    }
}
