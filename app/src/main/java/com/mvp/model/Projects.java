package com.mvp.model;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by manyeon
 */

@AutoValue
public abstract class Projects implements Parcelable {

    /*
    {
        "count":1,
        "next":null,
        "previous":null,
        "results":
        [
            {
                "id":1,
                "title":"testproject",
                "description":null,
                "due_date":"2016-05-02",
                "is_repeat":false,
                "is_privacy":true,
                "is_same":false,
                "minimum_amount":0,
                "goal":100,
                "now_amount":0,
                "project_status":0,     // No use
                "background_file":null,
                "created":"2016 Apr 27 11:27 AM",
                "username":"manyeon@cream.nyc",
                "user_id":7,
                "is_payment":false,
                "is_expiration":true
                "remaining_dates":0,
                "is_only_admin_show_user_list":false,
                "is_only_admin_show_transaction_list":false,
                "is_bookmarked":false,
                "created_bookmarked":"2016 05 13 03:35 PM",
                "pu_project":
                [
                    {
                        "id":117,
                        "project_id":117,
                        "is_payment":false,
                        "user_id":7,
                        "thumnail_file":"http://mud-kage.kakao.co.kr/14/dn/btqcOoH8lnY/TQElyHStMgIIek1Gj60ZYk/o.jpg",
                        "amount":0,
                        "username":"manyeon@cream.nyc",
                        "first_name":"manyeon"
                        "joined_date":"2016 05 13 03:35 PM"
                    }
                ],
                 "deposit_withdraw_refund":
                 [
                    10000,
                    0,
                    0
                 ],
                 "total_transaction_amount":
                 {
                        "total_withdraw":2000,
                        "total_refund":0,
                        "total_deposit":5000,
                        "total_pending":0
                 },
                 “eventid”:1,
                 “event_detail”:
                 {
                    “title”:“Blanditiis Et Sit A Delectus Possimus”,
                    "title_en": "English title event",
                    “btn_text”:“티켓사러가기“,
                    ”content”:“20000원“,
                    ”btn_text_en”:“ZMDxJzwsuydpzXrdpRQC”,
                    “content_en”:“EXYpfCZJSUdIhjbDxIry”,
                    "image":"https://....",
                    "share_thumb_image":"https://",
                    “is_active”:true,
                 },
                 "is_admin_archive": false
             }
        ]
    }
    */

    @SerializedName("count")
    public abstract int count();

    @SerializedName("results")
    public abstract ArrayList<Result> results();

    public static TypeAdapter<Projects> typeAdapter(Gson gson) {
        return new AutoValue_Projects.GsonTypeAdapter(gson);
    }

}
