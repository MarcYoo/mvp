package com.mvp.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manyeon
 */

@AutoValue
public abstract class Result implements Parcelable {
    @SerializedName("title")
    public abstract String title();

    @Nullable
    @SerializedName("background_file")
    public abstract String backgroundUrl();


    public static TypeAdapter<Result> typeAdapter(Gson gson) {
        return new AutoValue_Result.GsonTypeAdapter(gson);
    }

}
