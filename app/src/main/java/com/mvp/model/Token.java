package com.mvp.model;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by manyeon
 */

@AutoValue
public abstract class Token implements Parcelable {
    /*
    {
        "user_id":145,
        "my_wallet":
            [
                {"id":14,"user":145,"balance":0,"deposit_account_id":null,"ksnet_deposit_account_id":3499,"deposit_card_id":42,"withdraw_account_id":1354,"created":"2017 04 13 19:06"}
            ],
        "access_token":"drLWLVnAosssU84PmzmtC5QwclUVFg",
        "is_first_event":false,
        "is_apns":false,
        "expires_in":2582000,
        "token_type":"Bearer",
        "is_gcm":true,
        "vendor_info":null,
        "is_vendor":false,
        "scope":"read write",
        "user_name":"bebopbop@naver.com",
        "refresh_token":"V6e934FpLn6ePiprtdtZDJ0YBnvoTI"
     }
     */

    @SerializedName("user_id")
    public abstract int userId();

    @SerializedName("access_token")
    public abstract String accessToken();

    @SerializedName("token_type")
    public abstract String tokenType();

    public static TypeAdapter<Token> typeAdapter(Gson gson) {
        return new AutoValue_Token.GsonTypeAdapter(gson);
    }

}

