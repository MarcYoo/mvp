package com.mvp.model.factory;

import com.google.gson.TypeAdapterFactory;
import com.ryanharter.auto.value.gson.GsonTypeAdapterFactory;

/**
 * Created by manyeon
 */

@GsonTypeAdapterFactory
public abstract class TokenGsonTypeAdapterFactory implements TypeAdapterFactory {
    public static TypeAdapterFactory create() {
        return new AutoValueGson_TokenGsonTypeAdapterFactory();
    }
}
