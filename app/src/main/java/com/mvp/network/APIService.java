package com.mvp.network;

import com.mvp.model.Projects;
import com.mvp.model.Token;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by manyeon
 */

public interface APIService {

    @FormUrlEncoded
    @POST("o/token/")
    Observable<Token> getToken(
            @Field("grant_type") String grantType,
            @Field("username") String username,
            @Field("social_type") String socialType,
            @Field("password") String password,
            @Field("thumbnail_file") String thumbnailFile,
            @Field("device_id") String deviceId
    );

    @GET("users/{user}/projects/")
    Observable<Projects> getProjects(@Path("user") int user);

}
