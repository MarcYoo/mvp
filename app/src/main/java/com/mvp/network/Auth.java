package com.mvp.network;

import android.text.TextUtils;
import android.util.Base64;

import com.mvp.setting.SettingConstants;
import com.mvp.setting.SettingManager;

/**
 * Created by yoomanyeon
 */

public class Auth {
    private SettingManager mSettingManager;

    public Auth(SettingManager settingManager) {
        mSettingManager = settingManager;
    }

    @Override
    public String toString() {
        if(TextUtils.isEmpty(mSettingManager.getStringPreference(SettingConstants.PREFERENCE_AUTH, SettingConstants.DEFAULT_STRING_VALUE))) {
            String credentials = String.format("%s:%s", NetworkConstants.USERNAME, NetworkConstants.PASSWORD);
            return String.format("Basic %s", Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP));
        } else {
            return mSettingManager.getStringPreference(SettingConstants.PREFERENCE_AUTH, SettingConstants.DEFAULT_STRING_VALUE);
        }
    }
    
}
