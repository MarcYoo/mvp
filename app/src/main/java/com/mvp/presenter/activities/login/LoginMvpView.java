package com.mvp.presenter.activities.login;

import com.mvp.presenter.interfaces.MvpView;

/**
 * Created by manyeon
 */

public interface LoginMvpView extends MvpView {
    void showProgress();
    void hideProgress();
    void showMessage(int text);
    void nextActivity();
}
