package com.mvp.presenter.activities.login;

import android.os.Handler;

import com.mvp.R;
import com.mvp.injection.scope.AuthRetrofit;
import com.mvp.model.Token;
import com.mvp.network.APIService;
import com.mvp.presenter.base.BasePresenter;
import com.mvp.setting.SettingConstants;
import com.mvp.setting.SettingManager;
import com.mvp.util.DevLog;
import com.mvp.util.UtilConstants;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by manyeon
 */

public class LoginPresenter extends BasePresenter<LoginMvpView> {
    private static final String TAG = LoginPresenter.class.getName();
    private Disposable mDisposable;
    Retrofit mRetrofit;
    SettingManager mSettingManager;

    @Override
    public void attachView(LoginMvpView mvpView) {
        super.attachView(mvpView);

        if(mSettingManager.getBooleanPreference(SettingConstants.PREFERENCE_LOGIN, SettingConstants.DEFAULT_BOOLEAN_VALUE)) {
            getMvpView().nextActivity();
        }
    }

    @Override
    public void detachView() {
        super.detachView();
        if(mDisposable != null) {
            mDisposable.dispose();
        }
    }

    // Constructor Inject
    @Inject
    public LoginPresenter(@AuthRetrofit Retrofit retrofit, SettingManager settingManager) {
        mRetrofit = retrofit;
        mSettingManager = settingManager;
    }

    public void getToken(String grantType, String username, String socialType, String password, String thumbnailFile, String deviceId) {
        checkViewAttached();
        getMvpView().showProgress();
        mRetrofit.create(APIService.class).getToken(grantType, username, socialType, password, thumbnailFile, deviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Token>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mDisposable = d;
                    }

                    @Override
                    public void onNext(@NonNull Token response) {
                        mSettingManager.setIntegerPreference(SettingConstants.PREFERENCE_USER_ID, response.userId());
                        String auth = String.format("%s %s", response.tokenType(), response.accessToken());
                        mSettingManager.setStringPreference(SettingConstants.PREFERENCE_AUTH, auth);

                        DevLog.Logging(TAG, "USER ID: " + response.userId());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getMvpView().hideProgress();
                        getMvpView().showMessage(R.string.error);
                        DevLog.Logging(TAG, e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        mSettingManager.setBooleanPreference(SettingConstants.PREFERENCE_LOGIN, true);
                        getMvpView().hideProgress();
                        getMvpView().showMessage(R.string.success_login);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getMvpView().nextActivity();
                            }
                        }, UtilConstants.DELAY_MILLISECOND_500);

                    }
                });
    }
}
