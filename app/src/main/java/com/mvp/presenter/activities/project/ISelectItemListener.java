package com.mvp.presenter.activities.project;

import android.view.View;

import com.mvp.model.ItemModel;
import com.mvp.model.Result;

/**
 * Created by manyeon
 */

public interface ISelectItemListener {
    void onSelectItemListener(ItemModel itemModel);
}
