package com.mvp.presenter.activities.project;

import com.mvp.Data;
import com.mvp.model.ItemModel;
import com.mvp.presenter.interfaces.MvpView;

import java.util.ArrayList;

/**
 * Created by manyeon
 */

public interface ItemMvpView extends MvpView {
    void setActionBar(String title);
    void setListItem(ArrayList<ItemModel> itemModels);
    void setImageView(String url);
    void setBottomSheetLayout();
    void showBottomSheet(ItemModel model);
}
