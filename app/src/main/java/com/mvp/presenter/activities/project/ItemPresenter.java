package com.mvp.presenter.activities.project;

import com.mvp.Data;
import com.mvp.model.ItemModel;
import com.mvp.presenter.base.BasePresenter;
import com.mvp.util.UtilityManager;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

/**
 * Created by manyeon
 */

public class ItemPresenter extends BasePresenter<ItemMvpView> implements ISelectItemListener {

    private static final String TAG = ItemPresenter.class.getName();
    private Disposable mDisposable;
    UtilityManager mUtilityManager;

    @Override
    public void attachView(ItemMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if(mDisposable != null) {
            mDisposable.dispose();
        }
    }

    // Constructor Inject
    @Inject
    public ItemPresenter(UtilityManager utilityManager) {
        mUtilityManager = utilityManager;
    }

    public void setListItem() {
        checkViewAttached();
        getMvpView().setListItem(Data.getData());
    }

    @Override
    public void onSelectItemListener(ItemModel itemModel) {
        getMvpView().showBottomSheet(itemModel);
    }
}
