package com.mvp.presenter.activities.projects;

import android.view.View;

import com.mvp.model.Result;

/**
 * Created by manyeon
 */

public interface ISelectItemListener {
    void onSelectItemListener(Result result, View view);
}
