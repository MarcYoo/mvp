package com.mvp.presenter.activities.projects;

import android.view.View;

import com.mvp.model.Result;
import com.mvp.presenter.interfaces.MvpView;

import java.util.ArrayList;

/**
 * Created by manyeon
 */

public interface ProjectsMvpView extends MvpView {
    void showProgress();
    void hideProgress();
    void setItems(ArrayList<Result> items);
    void showMessage(int text);
    void moveProject(Result result, View view);
}
