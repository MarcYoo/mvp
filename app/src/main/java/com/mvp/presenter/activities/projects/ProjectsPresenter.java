package com.mvp.presenter.activities.projects;

import android.view.View;

import com.mvp.R;
import com.mvp.injection.scope.ProjectsRetrofit;
import com.mvp.model.Projects;
import com.mvp.model.Result;
import com.mvp.network.APIService;
import com.mvp.presenter.base.BasePresenter;
import com.mvp.setting.SettingConstants;
import com.mvp.setting.SettingManager;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by manyeon
 */

public class ProjectsPresenter extends BasePresenter<ProjectsMvpView> implements ISelectItemListener {

    private static final String TAG = ProjectsPresenter.class.getName();
    private Disposable mDisposable;
    Retrofit mRetrofit;
    SettingManager mSettingMangaer;

    @Override
    public void attachView(ProjectsMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if(mDisposable != null) {
            mDisposable.dispose();
        }
    }

    // Constructor Inject
    @Inject
    public ProjectsPresenter(@ProjectsRetrofit Retrofit retrofit, SettingManager settingManager) {
        mRetrofit = retrofit;
        mSettingMangaer = settingManager;
    }

    @Override
    public void onSelectItemListener(Result result, View view) {
        getMvpView().moveProject(result, view);
    }

    public void getProjects() {
        checkViewAttached();
        getMvpView().showProgress();
        mRetrofit.create(APIService.class).getProjects(mSettingMangaer.getIntegerPreference(SettingConstants.PREFERENCE_USER_ID, SettingConstants.DEFAULT_INTEGER_VALUE))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Projects>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        mDisposable = d;
                    }

                    @Override
                    public void onNext(@NonNull Projects projects) {
                        ArrayList<Result> results = projects.results();
                        if(results.isEmpty()) {
                            getMvpView().showMessage(R.string.empty_item);
                        } else {
                            getMvpView().setItems(results);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getMvpView().hideProgress();
                    }

                    @Override
                    public void onComplete() {
                        getMvpView().hideProgress();
                    }
                });
    }
}
