package com.mvp.presenter.base;

import com.mvp.presenter.interfaces.MvpView;
import com.mvp.presenter.interfaces.Presenter;

/**
 * Created by manyeon
 */

public class BasePresenter<T extends MvpView> implements Presenter<T> {
    private T mMvpView;

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public T getMvpView() {
        return mMvpView;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public void checkViewAttached() {
        if(isViewAttached() == false) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Call Presenter.attachView before requesting data to Presenter");
        }
    }
}
