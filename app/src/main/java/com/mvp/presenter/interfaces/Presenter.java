package com.mvp.presenter.interfaces;

/**
 * Created by manyeon
 */

public interface Presenter<V extends MvpView> {
    void attachView(V mvpView);
    void detachView();
}
