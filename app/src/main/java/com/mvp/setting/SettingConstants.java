package com.mvp.setting;

/**
 * Created by manyeon
 */

public class SettingConstants {
    public static final String DEFAULT_STRING_VALUE = "";
    public static final int DEFAULT_INTEGER_VALUE = 0;
    public static final boolean DEFAULT_BOOLEAN_VALUE = false;

    public static final String PREFERENCE_USER_ID = "preference_user_id";
    public static final String PREFERENCE_AUTH = "preference_auth";
    public static final String PREFERENCE_LOGIN = "preference_login";
}
