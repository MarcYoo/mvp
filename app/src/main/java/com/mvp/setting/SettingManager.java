package com.mvp.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mvp.MVP;

/**
 * Created by manyeon
 */

public class SettingManager {
    private static SharedPreferences mSharedPreferences = null;
    private Context mContext;

    public SettingManager(Context conext) {
        mContext = conext;
    }

    public SharedPreferences getSharedPreferences() {
        if(mSharedPreferences == null) {
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(MVP.get(mContext));
        }

        return mSharedPreferences;
    }

    public void setStringPreference(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringPreference(String key, String defaultValue) {
        return getSharedPreferences().getString(key, defaultValue);
    }

    public void setIntegerPreference(String key, int value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getIntegerPreference(String key, int defaultValue) {
        return getSharedPreferences().getInt(key, defaultValue);
    }

    public void setBooleanPreference(String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBooleanPreference(String key, boolean defaultValue) {
        return getSharedPreferences().getBoolean(key, defaultValue);
    }
}
