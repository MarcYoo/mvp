package com.mvp.util;

import android.util.Log;

/**
 * Created by manyeon
 */

public class DevLog {
    public static final boolean DEBUG = true;
    public static final boolean BODY = true;

    public static void Logging(String tag, String message) {
        if(DEBUG) {
            Log.d("<<< " + tag + " >>>", message);
        }
    }

    public static void LoggingError(String tag, String message) {
        if(DEBUG) {
            Log.e("<<< " + tag + " >>>", message);
        }
    }

}
