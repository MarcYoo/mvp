package com.mvp.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.mvp.KeyConstants;
import com.mvp.MVP;
import com.mvp.R;
import com.mvp.injection.module.ItemActivityModule;
import com.mvp.model.ItemModel;
import com.mvp.presenter.activities.project.ISelectItemListener;
import com.mvp.presenter.activities.project.ItemMvpView;
import com.mvp.presenter.activities.project.ItemPresenter;
import com.mvp.view.adapters.ItemAdapter;
import com.mvp.view.base.BaseActivity;
import com.mvp.view.fragments.CookpadFragment;
import com.mvp.view.widget.ImageZoomHelper;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yoomanyeon
 */

public class ItemActivity extends BaseActivity implements ItemMvpView {

    @BindView(R.id.bottomsheetLayout) BottomSheetLayout mBottomSheetLayout;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.transitionView) ImageView mTransitionView;
    @BindView(R.id.recyclerView) RecyclerView mRecyclerView;
    @BindView(R.id.focusStealer) View mFocusStealer;

    @Inject
    ItemPresenter mPresenter;

    private ImageZoomHelper mImageZoomHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_project);
        ButterKnife.bind(this);
        mPresenter.attachView(this);

        Intent intent = getIntent();
        String url = intent.getStringExtra(KeyConstants.URL_KEY);
        String title = intent.getStringExtra(KeyConstants.TEXT_KEY);

        setActionBar(title);
        setImageView(url);
        setBottomSheetLayout();
        mPresenter.setListItem();

        mImageZoomHelper = new ImageZoomHelper(this);

        Slide slide = new Slide(Gravity.BOTTOM);
        slide.addTarget(R.id.transitionView);
        getWindow().setEnterTransition(slide);

    }

    @Override
    protected void setupActivityComponent() {
        MVP.get(this)
                .getApplicationComponent()
                .plus(new ItemActivityModule(this))
                .inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        switch(mBottomSheetLayout.getState()) {
            case HIDDEN:
                removeRecyclerView();
                finishAfterTransition();
                break;
            case PREPARING:
                removeRecyclerView();
                finishAfterTransition();
                break;
            case PEEKED:
                mBottomSheetLayout.dismissSheet();
                break;
            case EXPANDED:
                mBottomSheetLayout.dismissSheet();
                break;
            default:
                removeRecyclerView();
                finishAfterTransition();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                removeRecyclerView();
                finishAfterTransition();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setActionBar(String title) {
        mToolbar.setTitle(title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void setListItem(ArrayList<ItemModel> itemModels) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new ItemAdapter(itemModels, onSelectItemListener));
    }

    @Override
    public void setImageView(String url) {
        mImageZoomHelper.setViewZoomable(mTransitionView);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.color.colorPrimary);
        requestOptions.error(R.color.colorPrimary);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(this).setDefaultRequestOptions(requestOptions).load(url).into(mTransitionView);

    }

    @Override
    public void setBottomSheetLayout() {
        mBottomSheetLayout.setShouldDimContentView(true);
        mBottomSheetLayout.setPeekOnDismiss(true);
        mBottomSheetLayout.setPeekSheetTranslation(getResources().getDimensionPixelSize(R.dimen.header_height_peeked));
        mBottomSheetLayout.setInterceptContentTouch(true);
    }

    @Override
    public void showBottomSheet(ItemModel model) {
        mFocusStealer.requestFocus();
        CookpadFragment fragment = CookpadFragment.newInstance(model);
        fragment.setBottomSheetLayout(mBottomSheetLayout);
        fragment.show(getSupportFragmentManager(), R.id.bottomsheetLayout);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(mBottomSheetLayout.isSheetShowing() == false) {
            return mImageZoomHelper.onDispatchTouchEvent(ev) || super.dispatchTouchEvent(ev);
        }

        return super.dispatchTouchEvent(ev);
    }

    private void removeRecyclerView() {
        mRecyclerView.setVisibility(View.GONE);
    }

    ISelectItemListener onSelectItemListener = new ISelectItemListener() {
        @Override
        public void onSelectItemListener(ItemModel model) {
            mPresenter.onSelectItemListener(model);
        }
    };

}
