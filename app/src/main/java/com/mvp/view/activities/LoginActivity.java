package com.mvp.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.mvp.MVP;
import com.mvp.R;
import com.mvp.injection.module.LoginActivityModule;
import com.mvp.presenter.activities.login.LoginMvpView;
import com.mvp.presenter.activities.login.LoginPresenter;
import com.mvp.view.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;

/**
 * Created by manyeon
 */

public class LoginActivity extends BaseActivity implements LoginMvpView {

    @BindView(R.id.button) Button mLoginButton;
    @BindView(R.id.message) TextView mMessage;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;

    @Inject
    LoginPresenter mPresenter;

    Disposable mDisposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mPresenter.attachView(this);
        onEvent();
    }

    @Override
    protected void setupActivityComponent() {
        MVP.get(this)
                .getApplicationComponent()
                .plus(new LoginActivityModule(this))
                .inject(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
        if(mDisposable != null) {
            mDisposable.dispose();
        }
    }

    @Override
    public void showProgress() {
        mLoginButton.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mLoginButton.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(int text) {
        mMessage.setText(text);
    }

    @Override
    public void nextActivity() {
        Intent intent = new Intent(LoginActivity.this, ProjectsActivity.class);
        startActivity(intent);
        finish();
    }

    private void onEvent() {
        mDisposable = RxView.clicks(mLoginButton).subscribe(event -> mPresenter.getToken(
                "password",
                "social_signin",
                "1",
                "yaIdpgfAeto0gy7ETPTBkg==",
                "http://th-p.talk.kakao.co.kr/th/talkp/wkJr5HyWZH/4lqS8hHoszcJI9t9OEsj0k/ynsvik_110x110_c.jpg",
                "c5+K9GsJOk4YcOkL6+h0QQ=="
        ));
    }
}

