package com.mvp.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mvp.KeyConstants;
import com.mvp.MVP;
import com.mvp.R;
import com.mvp.injection.module.ProjectsActivityModule;
import com.mvp.model.Result;
import com.mvp.presenter.activities.projects.ISelectItemListener;
import com.mvp.presenter.activities.projects.ProjectsMvpView;
import com.mvp.presenter.activities.projects.ProjectsPresenter;
import com.mvp.view.adapters.MainAdapter;
import com.mvp.view.base.BaseActivity;
import com.mvp.view.widget.GridRecyclerView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectsActivity extends BaseActivity implements ProjectsMvpView {

    @BindView(R.id.recyclerView) GridRecyclerView mRecyclerView;
    @BindView(R.id.message) TextView mMessage;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;

    @Inject
    ProjectsPresenter mPresenter;

    private ArrayList<Result> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter.attachView(this);

        if(savedInstanceState != null) {
            mItems = (ArrayList<Result>)savedInstanceState.getSerializable(KeyConstants.STATE_KEY);
            if(mItems != null && mItems.size() > 0) {
                setItems(mItems);
            } else {
                showMessage(R.string.empty_item);
            }
        } else {
            mPresenter.getProjects();
        }
    }

    @Override
    protected void setupActivityComponent() {
        MVP.get(this)
                .getApplicationComponent()
                .plus(new ProjectsActivityModule(this))
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KeyConstants.STATE_KEY, mItems);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    ISelectItemListener onSelectItemListener = new ISelectItemListener() {
        @Override
        public void onSelectItemListener(Result result, View view) {
            mPresenter.onSelectItemListener(result, view);
        }
    };

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setItems(ArrayList<Result> items) {
        mItems = items;
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(new MainAdapter(items, onSelectItemListener));
    }


    @Override
    public void showMessage(int text) {
        mMessage.setText(text);
    }

    @Override
    public void moveProject(Result result, View view) {
        Pair participants = new Pair<>(view, ViewCompat.getTransitionName(view));
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(ProjectsActivity.this, participants);
        Intent intent = new Intent(ProjectsActivity.this, ItemActivity.class);
        intent.putExtra(KeyConstants.URL_KEY, result.backgroundUrl());
        intent.putExtra(KeyConstants.TEXT_KEY, result.title());
        startActivity(intent, options.toBundle());
    }

}
