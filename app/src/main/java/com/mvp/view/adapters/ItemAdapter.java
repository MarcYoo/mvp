package com.mvp.view.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mvp.R;
import com.mvp.model.ItemModel;
import com.mvp.presenter.activities.project.ISelectItemListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manyeon
 */

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemModel> mItems;
    private ISelectItemListener mSelectItemListener;
    private Context mContext;

    public ItemAdapter(List<ItemModel> items, ISelectItemListener selectItemListener) {
        mItems = items;
        mSelectItemListener = selectItemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.row_detail_item, null);
        itemView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));

        ListViewHolder viewHolder = new ListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ListViewHolder itemController = (ListViewHolder)holder;
        ItemModel model = mItems.get(position);

        itemController.message.setText(model.getComment());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.color.colorPrimary);
        requestOptions.error(R.color.colorPrimary);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(model.getThumbnailUrl()).into(itemController.detailIcon);

        itemController.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectItemListener.onSelectItemListener(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardView) CardView cardView;
        @BindView(R.id.container) LinearLayout container;
        @BindView(R.id.detailIcon) ImageView detailIcon;
        @BindView(R.id.message) TextView message;

        public ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
