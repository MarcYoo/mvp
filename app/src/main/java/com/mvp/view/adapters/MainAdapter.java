package com.mvp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.mvp.R;
import com.mvp.model.Result;
import com.mvp.presenter.activities.projects.ISelectItemListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manyeon
 */

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Result> mItems;
    private ISelectItemListener mSelectItemListener;
    private Context mContext;

    public MainAdapter(List<Result> items, ISelectItemListener selectItemListener) {
        mItems = items;
        mSelectItemListener = selectItemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.row_item, null);
        ListViewHolder viewHolder = new ListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ListViewHolder itemController = (ListViewHolder)holder;
        Result result = mItems.get(position);

        String url = result.backgroundUrl() != null ? result.backgroundUrl() : "";

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.color.colorPrimary);
        requestOptions.error(R.color.colorPrimary);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(url).into(itemController.feedIcon);

        itemController.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectItemListener.onSelectItemListener(result, itemController.feedIcon);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.container) RelativeLayout container;
        @BindView(R.id.feedIcon) ImageView feedIcon;

        public ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
