package com.mvp.view.fragments;

enum BottomSheetState {
    HIDDEN, HIDDEN_PEEKED, PEEKED, PEEKED_EXPANDED, EXPANDED
}
