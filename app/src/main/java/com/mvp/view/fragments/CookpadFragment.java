package com.mvp.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.flipboard.bottomsheet.BaseViewTransformer;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.BottomSheetLayout.State;
import com.flipboard.bottomsheet.ViewTransformer;
import com.flipboard.bottomsheet.commons.BottomSheetFragment;
import com.mvp.R;
import com.mvp.model.ItemModel;
import com.mvp.view.activities.ItemActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by yoomanyeon
 */

public class  CookpadFragment extends BottomSheetFragment {

    @BindView(R.id.parent) FrameLayout parent;
    @BindView(R.id.nestedScrollView) NestedScrollView nestedScrollView;
    @BindView(R.id.appBarLayout) AppBarLayout mAppBarLayout;
    @BindView(R.id.bottomsheetContentView) FrameLayout mBottomsheetContentView;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.imageView) ImageView mBackgroundImageView;
    @BindView(R.id.cardContents) LinearLayout cardContents;
    @BindView(R.id.submessage) TextView submessage;

    private static ItemModel mModel;
    private BottomSheetLayout mBottomSheetLayout;
    private int mBottomSheetHeightPeeked, mBottomSheetHeightExpanded;
    private int mMovingImageviewSize;
    private int mActionBarHeight;

    private BottomSheetState mBottomSheetState = BottomSheetState.HIDDEN;

    public static CookpadFragment newInstance(ItemModel model) {
        CookpadFragment f = new CookpadFragment();
        mModel = model;
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cookpad, container, false);
        view.setMinimumHeight(getResources().getDisplayMetrics().heightPixels);
        ButterKnife.bind(this, view);
        initValue();
        setSize();
        setInitialize();
        event();

        return view;
    }

    private void initValue() {
        mBottomSheetHeightPeeked = (int)getResources().getDimension(R.dimen.header_height_peeked);
        mBottomSheetHeightExpanded = Math.max(mBottomSheetHeightPeeked, (int)getResources().getDimension(R.dimen.header_height_expanded));
        mMovingImageviewSize = (int)getResources().getDimension(R.dimen.moving_image_collapsed_bottom_sheet_size);
        mActionBarHeight = getActionBarHeight(getActivity());
    }

    private void setSize() {
        mAppBarLayout.getLayoutParams().height = mBottomSheetHeightExpanded;
        ((ViewGroup.MarginLayoutParams)mBottomsheetContentView.getLayoutParams()).topMargin = mMovingImageviewSize / 2;
        ((ViewGroup.MarginLayoutParams)mBottomsheetContentView.getLayoutParams()).height = mBottomSheetHeightPeeked - mMovingImageviewSize / 2;
    }

    private void setInitialize() {
        mToolbar.setTitle("");
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        parent.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, deviceHeight()));

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.color.colorPrimary);
        requestOptions.error(R.color.colorPrimary);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

        String url = mModel.getThumbnailUrl();
        Glide.with(this).setDefaultRequestOptions(requestOptions).load(url).into(mBackgroundImageView);

        cardContents.removeAllViews();
        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i = 0; i < 7; ++i) {
            View view = inflater.inflate(R.layout.cardview_layout, null);
            view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
            ((TextView)view.findViewById(R.id.title)).setText(mModel.getComment());
            ((TextView)view.findViewById(R.id.description)).setText(mModel.getDescription());
            cardContents.addView(view);
        }

        submessage.setText(Html.fromHtml(getString(R.string.card_content)));

    }

    private void event() {
        mBottomsheetContentView.setOnClickListener(onClickContentView);
        mToolbar.setNavigationOnClickListener(onClickToolbar);
        mAppBarLayout.addOnOffsetChangedListener(onOffsetChangedListener);
    }

    public void setBottomSheetLayout(BottomSheetLayout bottomSheetLayout) {
        mBottomSheetLayout = bottomSheetLayout;

        mBottomSheetLayout.addOnSheetStateChangeListener(new BottomSheetLayout.OnSheetStateChangeListener() {

            State lastState;

            @Override
            public void onSheetStateChanged(State state) {
                if(lastState == state || !isAdded())
                    return;

                lastState = state;

            }
        });
    }

    @Override
    public ViewTransformer getViewTransformer() {
        final float targetSizeWhenBottomSheetExpanded = getResources().getDimensionPixelSize(R.dimen.moving_image_expanded_bottom_sheet_size);
        final float startSizeWhenBottomSheetCollapsed = getResources().getDimensionPixelSize(R.dimen.moving_image_collapsed_bottom_sheet_size);
        return new BaseViewTransformer() {
            boolean init = false;
            float mOriginalBottomSheetBackgroundImageViewY;
            float scaleDiff;

            @Override
            public void transformView(final float translation, final float maxTranslation, final float peekedTranslation, final BottomSheetLayout parent, final View view) {
                if (!init) {
                    mOriginalBottomSheetBackgroundImageViewY = mMovingImageviewSize / 2;
                    scaleDiff = (startSizeWhenBottomSheetCollapsed - targetSizeWhenBottomSheetExpanded) / startSizeWhenBottomSheetCollapsed;
                    init = true;
                }

                if (translation < mBottomSheetHeightPeeked) {
                    // hidden <-> peeked or hidden
                    reportState(translation == 0 ? BottomSheetState.HIDDEN : BottomSheetState.HIDDEN_PEEKED);
                    return;
                }
                if (translation == mBottomSheetHeightPeeked) {
                    // peek
                    reportState(BottomSheetState.PEEKED);
                } else {
                    // peek -> expand
                    reportState(translation == maxTranslation ? BottomSheetState.EXPANDED : BottomSheetState.PEEKED_EXPANDED);
                }
            }
        };
    }

    protected void reportState(final BottomSheetState state) {
        if (mBottomSheetState != state) {
            mBottomSheetState = state;
            switch (state) {
                case HIDDEN:
                    break;
                case HIDDEN_PEEKED:
                    break;
                case PEEKED:
                    mBackgroundImageView.animate().cancel();
                    final ViewPropertyAnimator animator = mBackgroundImageView.animate();
                    animator.setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    break;
                case PEEKED_EXPANDED:
                    mBackgroundImageView.animate().cancel();
                    mBackgroundImageView.animate().alpha(1);
                    break;
                case EXPANDED:
                    break;
            }
        }
    }

    View.OnClickListener onClickContentView = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mBottomSheetLayout.expandSheet();
        }
    };

    View.OnClickListener onClickToolbar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(mBottomSheetLayout != null) {
                mBottomSheetLayout.dismissSheet();
            } else {
                ((ItemActivity)getActivity()).onBackPressed();
            }
        }
    };

    AppBarLayout.OnOffsetChangedListener onOffsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
        boolean init = false;
        float startY, scaleDiff, startScale, targetY;

        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if(mBottomSheetLayout != null && mBottomSheetLayout.isSheetShowing() && mBottomSheetLayout.getState() == BottomSheetLayout.State.EXPANDED) {
                if(!init) {
                    final int startMarginBottom = getResources().getDimensionPixelSize(R.dimen.header_view_start_margin_bottom);
                    final float targetSizeWhenBottomSheetExpanded = getResources().getDimensionPixelSize(R.dimen.moving_image_expanded_bottom_sheet_size);
                    final float startSizeWhenBottomSheetCollapsed = getResources().getDimensionPixelSize(R.dimen.moving_image_collapsed_bottom_sheet_size);
                    scaleDiff = (startSizeWhenBottomSheetCollapsed - targetSizeWhenBottomSheetExpanded) / startSizeWhenBottomSheetCollapsed;

                    startY = mBottomSheetHeightExpanded - startMarginBottom;
                    final float actionBarIconPadding = getResources().getDimensionPixelSize(R.dimen.moving_image_collapsed_toolbar_top_and_bottom_padding);
                    final float targetImageViewSize = mActionBarHeight - actionBarIconPadding * 2;
                    targetY = mBottomSheetHeightExpanded - actionBarIconPadding - targetImageViewSize;
                    final float targetScale = targetImageViewSize / mMovingImageviewSize;
                    scaleDiff = startScale - targetScale;
                    init = true;
                }

            }
        }
    };

    public int getActionBarHeight(final Activity activity) {
        final TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (activity.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources()
                    .getDisplayMetrics());
        return actionBarHeight;
    }

    private int deviceHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return metrics.heightPixels - getStatusBarHeight();
    }

    private int getStatusBarHeight() {
        int statusBarHeight = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if(resourceId > 0) {
            statusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        return statusBarHeight;
    }
}
